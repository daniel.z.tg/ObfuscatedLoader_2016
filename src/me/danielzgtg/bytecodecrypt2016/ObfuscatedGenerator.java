package me.danielzgtg.bytecodecrypt2016;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class ObfuscatedGenerator {

	public static void main(String[] args) {
		System.out.println("Generating...");
		
		try {
			
			byte[] b = Files.readAllBytes(Paths.get("HelloWorld.class"));
			b = Base64.getEncoder().encode(b);
			Files.write(Paths.get("HelloWorld.class.obf"), b);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Done");
		
	}
}
