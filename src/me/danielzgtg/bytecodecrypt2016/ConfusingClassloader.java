package me.danielzgtg.bytecodecrypt2016;

import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class ConfusingClassloader extends ClassLoader {

	private Map<String, byte[]> obfuscatedClassCache = new HashMap<>();

	public void cacheClass(final String name, final byte[] data) {
		if (!this.obfuscatedClassCache.containsKey(name)) {
			this.obfuscatedClassCache.put(name, Arrays.copyOf(data, data.length));
		} else {
			throw new IllegalStateException("Class already cached!: " + name);
		}
	}

	private byte[] decrypt(final byte[] b) {
		return Base64.getDecoder().decode(b);
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		byte[] data = this.obfuscatedClassCache.get(name);

		if (data == null) {
			throw new ClassNotFoundException();
		}

		byte[] decrypted = decrypt(data);

		return this.defineClass(name, decrypted, 0, decrypted.length);
	}

}
