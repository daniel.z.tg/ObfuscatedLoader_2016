package me.danielzgtg.bytecodecrypt2016;

import java.nio.file.Files;
import java.nio.file.Paths;

public class ObfuscatedMain {

	public static void main(String[] args) {
		try {
			ConfusingClassloader ccl = new ConfusingClassloader();
			
			ccl.cacheClass("HelloWorld", Files.readAllBytes(Paths.get("HelloWorld.class.obf")));
			Class<?> clazz = ccl.loadClass("HelloWorld");
			clazz.getMethod("main", String[].class).invoke(null, (Object) null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
